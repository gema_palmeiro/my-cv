import {
  personalInfo,
  onLoadPersonalInfo,
  scrollToPersonalInfo,
} from "./scripts/personalInfo";
import {
  languages,
  onLoadLanguageInfo,
  scrollToLanguagues,
} from "./scripts/languages";
import { skillsInfo, onLoadSkillsInfo, scrollToSkills } from "./scripts/skills";
import {
  experienceInfo,
  onLoadExperienceInfo,
  scrollToExperience,
} from "./scripts/experience";
import{
	studiesInfo,
    onLoadStudiesInfo,
    scrollToStudies,
}from "./scripts/studies";
import "./styles.scss";

let addListeners = () => {
  document
    .getElementById("goToPersonalInfo")
    .addEventListener("click", scrollToPersonalInfo);
  document
    .getElementById("goToLanguages")
    .addEventListener("click", scrollToLanguagues);
  document
    .getElementById("goToSkills")
    .addEventListener("click", scrollToSkills);
  document
    .getElementById("goToExperience")
    .addEventListener("click", scrollToExperience);
document
    .getElementById("goToStudies")
    .addEventListener("click", scrollToStudies);
  //Anadir eventos de scroll a cada una de las secciones
};

window.onload = function () {
  // DOM update
  onLoadPersonalInfo();
  onLoadLanguageInfo();
  onLoadSkillsInfo();
  onLoadExperienceInfo();
  onLoadStudiesInfo();

  // Add listeners
  addListeners();
};
