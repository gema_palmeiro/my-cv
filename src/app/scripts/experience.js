// Crear una variable con array de objetos con vuestra experiencia que incluya el nombre del trabajo
//, descripción, fecha de inicio y fecha final
// Rellenar el contenedor de experiencia manipulando el DOM
const experienceInfo = [
    {
        nombre: "Webmaster",
        descripcion: "Operatividad, programación, actualización y mantenimiento web",
        fechainicio: new Date(2019,5,23).getFullYear(),
        fechafin: new Date(2020,6,15).getFullYear(),
    },
    {
        nombre: "Digital Content",
        descripcion: "SEO, programación, actualización y mantenimiento web",
        fechainicio: new Date(2016,4,15).getFullYear(),
        fechafin: new Date(2019,4,15).getFullYear(),
    },
    {
        nombre: "Webmaster",
        descripcion: "Programación, actualización y mantenimiento web",
        fechainicio: new Date(2014,5,18).getFullYear(),
        fechafin: new Date(2016,3,2).getFullYear(),
    },
    {
        nombre: "Webcontent Editor",
        descripcion: "Edición, copy, diseño y mantenimiento web",
        fechainicio: new Date(2010,4,16).getFullYear(),
        fechafin: new Date(2014,4,15).getFullYear(),
    },
];
let onLoadExperienceInfo = () => {
    let experienceData = document.getElementById("experienceInfoData");
    experienceInfo.forEach((element) =>{
        let newDivExperience = document.createElement("div");
        experienceData.appendChild(newDivExperience);
        newDivExperience.classList.add("container__card");
        let newH3Name = document.createElement("h3");
        newH3Name.innerHTML = element.nombre;
        newDivExperience.appendChild (newH3Name);
        let newPDescription =document.createElement("p");
        newPDescription.innerHTML = element.descripcion;
        newDivExperience.appendChild(newPDescription);
        let newPDataStart = document.createElement("p");
        newPDataStart.innerHTML = element.fechainicio;
        newDivExperience.appendChild(newPDataStart);
        let newPDataEnd = document.createElement("p");
        newPDataEnd.innerHTML = element.fechafin;
        newDivExperience.appendChild (newPDataEnd);
    });
}
let scrollToExperience = () => {
	let scrollExperienceContainer = document.getElementById("experience");
	scrollExperienceContainer.scrollIntoView({block: "end", behavior: "smooth"});
}
export {
	experienceInfo,
	onLoadExperienceInfo,
	scrollToExperience
}