const languages = {
	nameEstudies: "Inglés",
	certification: "Cambridge",
	level: "Advance",
  };
// Rellenar el contenedor de idiomas manipulando el DOM

let onLoadLanguageInfo = () => {
	let nameLanguageInfo = document.getElementById("languageName");
	let certificationLanguage = document.getElementById("languageCertification");
	let levelLanguage = document.getElementById("languageLevel");
	nameLanguageInfo.innerHTML = languages.nameEstudies;
	certificationLanguage.innerHTML = languages.certification;
	levelLanguage.innerHTML = languages.level;
}

let scrollToLanguagues = () => {
	let languageInfoContainer = document.getElementById("languages");
	languageInfoContainer.scrollIntoView({block: "end", behavior: "smooth"});
}

  
export {
	languages,
	onLoadLanguageInfo,
	scrollToLanguagues
}