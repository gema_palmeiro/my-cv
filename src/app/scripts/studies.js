// Crear una variable con array de objetos con vuestros estudios que incluya el nombre del estudio, descripción, fecha de inicio y fecha final
// Rellenar el contenedor de experiencia manipulando el DOM

const studiesInfo = [
  {
    nombre: "Master en I+D",
    descripcion: "Gestión de la innovación y gestión de proyectos",
    fechainicio: new Date(2014, 11, 11).getFullYear(),
    fechafin: new Date(2015, 2, 15).getFullYear(),
  },
  {
    nombre: "Licenciatura en Biologia",
    descripcion: "Carrera eminentemente científica que alterna la información de tipo técnico con la aplicación práctica",
    fechainicio: new Date(2001, 9, 11).getFullYear(),
    fechafin: new Date(2006, 9, 15).getFullYear(),
  },
  {
    nombre: "Analista informático",
    descripcion:"Aplicación de software, preparación de requisitos de software y los documentos de especificación",
    fechainicio: new Date(2005, 4, 12).getFullYear(),
    fechafin: new Date(2008, 10, 13).getFullYear(),
  },
];
let onLoadStudiesInfo = () => {
  let studiesData = document.getElementById("studiesInfoData");
  studiesInfo.forEach((element) => {
    let newDivStudy = document.createElement("div");
    studiesData.appendChild(newDivStudy);
    newDivStudy.classList.add("container__card");
    let newH3Name = document.createElement("h3");
    newH3Name.innerHTML = element.nombre;
    newDivStudy.appendChild(newH3Name);
    let newPDescription = document.createElement("p");
    newPDescription.innerHTML = element.descripcion;
    newDivStudy.appendChild(newPDescription);
    let newPDataStart = document.createElement("p");
    newPDataStart.innerHTML = element.fechainicio;
    newDivStudy.appendChild(newPDataStart); 
    let newPDataEnd = document.createElement("p");
    newPDataEnd.innerHTML = element.fechafin;
    newDivStudy.appendChild(newPDataEnd);
  });
}
let scrollToStudies = () => {
	let scrollStudiesContainer = document.getElementById("studies");
	scrollStudiesContainer.scrollIntoView({block: "end", behavior: "smooth"});
}
export{
    studiesInfo,
    onLoadStudiesInfo,
    scrollToStudies
}