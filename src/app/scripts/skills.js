// Crear un array de array de objetos que inclyan el nombre de la skill y el porcentaje de 0-100 del nivel en esa skill
const skillsInfo = [
  {
    name: "HTML",
    percent: "70",
  },
  {
    name: "CSS",
    percent: "50",
  },
  {
    name: "JavaScript",
    percent: "40",
  },
  {
    name: "MySQL",
    percent: "50",
  },
  {
    name: "BootStrap",
    percent: "30",
  },
];

let onLoadSkillsInfo = () => {
  let skillsInfoData = document.getElementById("skillsInfoData");
  skillsInfo.forEach((skill) => {
    let newPName = document.createElement("p");
    newPName.innerHTML=skill.name;
    skillsInfoData.appendChild(newPName);
    let newPPercent = document.createElement("progress");
    newPPercent.setAttribute('max', "100");
    newPPercent.setAttribute('value', skill.percent)
    skillsInfoData.appendChild(newPPercent);
  });
};

let scrollToSkills = () => {
	let scrollInfoContainer = document.getElementById("skills");
	scrollInfoContainer.scrollIntoView({block: "end", behavior: "smooth"});
}

export { skillsInfo, onLoadSkillsInfo, scrollToSkills };


