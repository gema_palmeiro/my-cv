const personalInfo = {
	id: "PersonalInfo",
	name: "Gema Palmeiro Molina",
	email: "gema.palmeiro@gmail.com",
	phone: 666998877,
	address: "Calle de la Piruleta",
	job: "Front-End developer",
	photo: "../assets/img/happiness.png ",
};

let onLoadPersonalInfo = () => {
	let namePersonalInfo = document.getElementById("name");
	let emailPersonalInfo = document.getElementById("email");
	let phonePersonalInfo = document.getElementById("phone");
	let addressPersonalInfo = document.getElementById("address");
	let photoPersonalInfo = document.getElementById("myPicture")
	namePersonalInfo.innerHTML = personalInfo.name;
	emailPersonalInfo.innerHTML = personalInfo.email;
	phonePersonalInfo.innerHTML = personalInfo.phone;
	addressPersonalInfo.innerHTML = personalInfo.address;
	/* Foto */
	let myPictureInfo =document.getElementById("myPicture");
	let Picture = document.createElement('IMG');
    Picture.setAttribute('src', personalInfo.photo);
    myPictureInfo.appendChild(Picture);
	/* Main Info Job */
	let myWorkInfo = document.getElementById("myWork")
	let work = document.createElement("h1");
	work.innerHTML = personalInfo.job;
	myWorkInfo.appendChild(work);
}


let scrollToPersonalInfo = () => {
	let personalInfoContainer = document.getElementById("personalInfo");
	personalInfoContainer.scrollIntoView({block: "end", behavior: "smooth"});
}

export {
	personalInfo,
	onLoadPersonalInfo,
	scrollToPersonalInfo
}